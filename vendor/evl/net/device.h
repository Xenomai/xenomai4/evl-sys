/*
 * SPDX-License-Identifier: MIT
 *
 * Copyright (C) 2024 Philippe Gerum  <rpm@xenomai.org>
 */

#ifndef _EVL_NET_DEVICE_H
#define _EVL_NET_DEVICE_H

#include <evl/net/net-abi.h>
#include <evl/net/device-abi.h>
#include <evl/net/bpf-abi.h>

#endif /* _EVL_NET_DEVICE_H */
