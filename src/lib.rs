#![allow(non_upper_case_globals)]
#![allow(non_camel_case_types)]
#![allow(non_snake_case)]

include!(concat!(env!("OUT_DIR"), "/bindings.rs"));

use bitflags::bitflags;
use core::mem::MaybeUninit;

bitflags! {
    #[derive(Default)]
    pub struct CloneFlags: u32 {
        const PRIVATE = 0;	      // EVL_CLONE_PRIVATE
        const PUBLIC = (1 << 16);     // EVL_CLONE_PUBLIC
        const OBSERVABLE = (1 << 17); // EVL_CLONE_OBSERVABLE
        const NONBLOCK = (1 << 18);   // EVL_CLONE_NONBLOCK
        const UNICAST = (1 << 19);    // EVL_CLONE_UNICAST
        const INPUT = (1 << 20);      // EVL_CLONE_INPUT
        const OUTPUT = (1 << 21);     // EVL_CLONE_OUTPUT
    }
}

bitflags! {
    #[derive(Default)]
    pub struct MutexType: u32 {
        const NORMAL = 0;           	// EVL_MUTEX_NORMAL
        const RECURSIVE = (1 << 0); 	// EVL_MUTEX_RECURSIVE
    }
}

#[derive(Debug, Copy, Clone, PartialEq)]
pub enum BuiltinClock {
    MONOTONIC = -libc::CLOCK_MONOTONIC as isize,
    REALTIME = -libc::CLOCK_REALTIME as isize,
}

// EVL-specific policies as defined in include/uapi/evl/sched.h.
pub const SCHED_WEAK: i32 = 43;
pub const SCHED_QUOTA: i32 = 44;
pub const SCHED_TP: i32 = 45;
// Re-export the standard libc policies.
pub const SCHED_FIFO: i32 = libc::SCHED_FIFO;
pub const SCHED_RR: i32 = libc::SCHED_RR;

#[derive(Debug, PartialEq)]
pub struct SchedOther;

#[derive(Debug, Copy, Clone, PartialEq)]
pub struct SchedFifo {
    pub prio: i32,
}

impl From<i32> for SchedFifo {
    fn from(prio: i32) -> Self {
        SchedFifo {
            prio
        }
    }
}

#[derive(Debug, Copy, Clone, PartialEq)]
pub struct SchedRR {
    pub prio: i32,
}

impl From<i32> for SchedRR {
    fn from(prio: i32) -> Self {
        SchedRR {
            prio
        }
    }
}

#[derive(Debug, Copy, Clone, PartialEq)]
pub struct SchedWeak {
    pub prio: i32,
}

impl From<i32> for SchedWeak {
    fn from(prio: i32) -> Self {
        SchedWeak {
            prio
        }
    }
}

#[derive(Debug, Copy, Clone, PartialEq)]
pub struct SchedQuota {
    pub group: i32,
    pub prio: i32,
}

impl From<(i32, i32)> for SchedQuota {
    fn from(tuple: (i32, i32)) -> Self {
        let (group, prio) = tuple;
        SchedQuota {
            group,
            prio
        }
    }
}

#[derive(Debug, Copy, Clone, PartialEq)]
pub struct SchedTP {
    pub part: i32,
    pub prio: i32,
}

impl From<(i32, i32)> for SchedTP {
    fn from(tuple: (i32, i32)) -> Self {
        let (part, prio) = tuple;
        SchedTP {
            part,
            prio
        }
    }
}

#[derive(Debug, PartialEq)]
pub enum SchedAttrs {
    OTHER(SchedOther),
    FIFO(SchedFifo),
    RR(SchedRR),
    WEAK(SchedWeak),
    QUOTA(SchedQuota),
    TP(SchedTP),
    UNKNOWN,
}

impl From<evl_sched_attrs> for SchedAttrs {
    fn from(attrs: evl_sched_attrs) -> Self {
        match attrs.sched_policy {
            libc::SCHED_OTHER => {
                SchedAttrs::OTHER(SchedOther)
            },
            libc::SCHED_FIFO => {
                SchedAttrs::FIFO(attrs.sched_priority.into())
            },
            libc::SCHED_RR => {
                SchedAttrs::RR(attrs.sched_priority.into())
            },
            SCHED_WEAK => {
                SchedAttrs::WEAK(attrs.sched_priority.into())
            },
            SCHED_QUOTA => {
                SchedAttrs::QUOTA(
                    (
                        unsafe { attrs.sched_u.quota.__sched_group },
                        attrs.sched_priority
                    ).into()
                )
            },
            SCHED_TP => {
                SchedAttrs::TP(
                    (
                        unsafe { attrs.sched_u.tp.__sched_partition },
                        attrs.sched_priority
                    ).into()
                )
            }
            _ => {
                SchedAttrs::UNKNOWN
            }
        }
    }
}

fn get_zero_attrs() -> evl_sched_attrs {
    unsafe {
        MaybeUninit::<evl_sched_attrs>::zeroed().assume_init()
    }
}

impl From<SchedAttrs> for evl_sched_attrs {
    fn from(attrs: SchedAttrs) -> Self {
        match attrs {
            SchedAttrs::OTHER(_) => {
                let mut x = get_zero_attrs();
                x.sched_policy = libc::SCHED_OTHER;
                x.sched_priority = 0;
                x
            },
            SchedAttrs::FIFO(fifo) => {
                let mut x = get_zero_attrs();
                x.sched_policy = libc::SCHED_FIFO;
                x.sched_priority = fifo.prio;
                x
            },
            SchedAttrs::RR(rr) => {
                let mut x = get_zero_attrs();
                x.sched_policy = libc::SCHED_RR;
                x.sched_priority = rr.prio;
                x
            },
            SchedAttrs::WEAK(weak) => {
                let mut x = get_zero_attrs();
                x.sched_policy = SCHED_WEAK;
                x.sched_priority = weak.prio;
                x
            },
            SchedAttrs::QUOTA(quota) => {
                let mut x = get_zero_attrs();
                x.sched_policy = SCHED_QUOTA;
                x.sched_priority = quota.prio;
                x.sched_u.quota.__sched_group = quota.group;
                x
            },
            SchedAttrs::TP(tp) => {
                let mut x = get_zero_attrs();
                x.sched_policy = SCHED_TP;
                x.sched_priority = tp.prio;
                x.sched_u.tp.__sched_partition = tp.part;
                x
            }
            _ => {
                SchedAttrs::UNKNOWN.into()
            }
        }
    }
}
